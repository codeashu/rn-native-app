import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header, Title, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import moment from 'moment';

export default class List extends Component {
  render() {
    return (
      <Container>
        <Header >
            <Left/>
            <Body>
                <Title>Card List</Title>
            </Body>
        <Right>
            <Button hasText transparent>
              <Text>Add</Text>
            </Button>
          </Right>
        </Header>
        <Content>
          <Card>
            <CardItem>
            <Text>Title: </Text><Text>Title</Text>
            </CardItem>
            <CardItem>
            <Text>Price: </Text><Text>30</Text>
            </CardItem>
            <CardItem>
            <Text>Date: </Text><Text>{moment().format("DD-MMM-YY")}</Text>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
            <Text>Title: </Text><Text>Title</Text>
            </CardItem>
            <CardItem>
            <Text>Price: </Text><Text>30</Text>
            </CardItem>
            <CardItem>
            <Text>Date: </Text><Text>{moment().format("DD-MMM-YY")}</Text>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
            <Text>Title: </Text><Text>Title</Text>
            </CardItem>
            <CardItem>
            <Text>Price: </Text><Text>30</Text>
            </CardItem>
            <CardItem>
            <Text>Date: </Text><Text>{moment().format("DD-MMM-YY")}</Text>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
            <Text>Title: </Text><Text>Title</Text>
            </CardItem>
            <CardItem>
            <Text>Price: </Text><Text>30</Text>
            </CardItem>
            <CardItem>
            <Text>Date: </Text><Text>{moment().format("DD-MMM-YY")}</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}