import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, Icon, Dimensions} from 'react-native';
import { RNCamera } from 'react-native-camera';

// import Camera from 'react-native-camera';
export default class TakePicture extends Component {
    render() {
        return  (
          <View style={styles.container}>
            <View style={{position:'absolute',borderColor:'red',borderWidth:4}} >
            <TouchableOpacity
                onPress={this.takePicture.bind(this)}
                style = {styles.capture}
            >
                <Text style={{fontSize: 14}}> save </Text>
            </TouchableOpacity>
            </View>
            <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                style = {styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.on}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We need your permission to use your camera phone'}
            />
          </View>
        );
      }
    
      takePicture = async function() {
        if (this.camera) {
          const options = { quality: 0.5, base64: true };
          const data = await this.camera.takePictureAsync(options)
          console.log(data.uri);
        }
      };
}

const styles = StyleSheet.create({
    container: {
        flex:1
      },
      preview: {
        flex: 1,
        // justifyContent: 'flex-end',
        alignItems: 'center'
      },
      capture: {
        height:50,
        width:60,
        position:'absolute',
        backgroundColor: 'lightblue',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
      }
});