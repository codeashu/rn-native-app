import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Button, Text, Icon, Left, Body, Right,Title } from 'native-base';
export default class Add extends Component {
  render() {
    return (
      <Container>
        <Header >
        <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Add Item</Title>
          </Body>
          <Right />
        </Header>
        <Content style={{marginRight:20}} >
          <Form>
            <Item>
              <Input placeholder="Name" />
            </Item>
            <Item>
              <Input placeholder="Price" />
            </Item>
            <Item>
              <Input placeholder="Description" />
            </Item>
            <Item>
              <Input placeholder="Expiration Date" />
            </Item>
            <Item>
              <Input placeholder="Upload Media" />
            <Icon active name='cloud-upload' style={{height:20,width:20}} />
            </Item>

            <Button style={{margin:20}}  block>
                <Text>Save</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}